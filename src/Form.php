<?php

namespace Drupal\money_extra;

use Drupal\mixin\Arrays;
use Drupal\mixin\Decimal;

class Form extends Base {

  static function hook_element_validate_limit($element, &$form_state) {
    $value = $element['#value'];
    $field = $form_state['values']['instance']['field_name'];
    $settings = &$form_state['field'][$field][LANGUAGE_NONE]['field']['settings'];

    if ($value != '' && (FALSE == is_numeric($value))) {
      form_error($element, t('%name must be a number.', ['%name' => $element['#title']]));
    }

    if ($value != '' && isset($settings)) {
      static::alterSettings($settings);
      $min = $settings['min'];
      $max = $settings['max'];
      $item = Decimal::create($value, $settings['scale']);
      $name = $element['#name'];
      $title = $element['#title'];

      if ($name == 'instance[settings][min]' && $item->compareWith('<', $min)) {
        form_error($element, t('%name: the value may be no less than %min.', ['%name' => $title, '%min' => $min]));
      }

      if ($name == 'instance[settings][max]' && $item->compareWith('>', $max)) {
        form_error($element, t('%name: the value may be no greater than %max.', ['%name' => $title, '%max' => $max]));
      }
    }
  }

  static function hook_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
    $module = &$form['#field']['module'];

    if ((FALSE == isset($module)) || ('money' !== $module)) {
      return;
    }

    $settings = &$form['instance']['settings'];
    $hook = static::getType()->getModule();
    $function = '_element_validate_limit';
    $callback = function ($index, $item) use ($hook, $function) {
      return ($item == $function) ? $hook . $function : $item;
    };

    foreach (['min', 'max'] as $item) {
      $validate = &$settings[$item]['#element_validate'];
      $validate = Arrays::map((array)$validate, $callback);
    }
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = ['form_field_ui_field_edit_form_alter', 'element_validate_limit'];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
