<?php

namespace Drupal\money_extra;

use Drupal\mixin\Traits\Hook;

abstract class Base {
  use Hook;

  protected static function alterSettings(&$settings) {
    $precision = $settings['precision'];
    $scale = $settings['scale'];
    $min = ('-' . str_repeat('9', $precision - $scale) . '.' . str_repeat('9', $scale));
    $max = (str_repeat('9', $precision - $scale) . '.' . str_repeat('9', $scale));
    $settings['min'] = $min;
    $settings['max'] = $max;
  }
}
