<?php

namespace Drupal\money_extra;

use Drupal\mixin\Decimal;

class Validate extends Base {
  protected static $options = ['default' => FALSE, 'deleted' => FALSE, 'language' => NULL];
  protected static $removes = ['money_min' => TRUE, 'money_max' => TRUE];
  protected $type;
  protected $entity;
  protected $errors;

  protected function __construct($entity_type, $entity, &$errors) {
    $this->type = $entity_type;
    $this->entity = $entity;
    $this->errors = &$errors;
  }

  static function hook_field_attach_validate($entity_type, $entity, &$errors) {
    static::build($entity_type, $entity, $errors)->doValidate();
  }

  function doValidate() {
    foreach ($this->getInstances() as $instance) {
      $field = field_info_field_by_id($instance['field_id']);

      if ('money' != $field['module']) {
        continue;
      }

      static::alterSettings($field['settings']);
      $name = $field['field_name'];
      $this->clearErrors($name);

      foreach ($this->getLanguages($field) as $language) {
        $this->validateInstance($field, $instance, $language);
      }
    }
  }

  protected function getInstances() {
    list(, , $bundle) = entity_extract_ids($this->type, $this->entity);
    return _field_invoke_get_instances($this->type, $bundle, static::$options);
  }

  protected function clearErrors($name) {
    $errors = &$this->errors[$name];

    if (FALSE == isset($errors)) {
      return;
    }

    foreach ($errors as $language => $deltas) {
      foreach ($deltas as $delta => $items) {
        foreach ($items as $index => $item) {
          if (isset(static::$removes[$item['error']])) {
            unset($errors[$language][$delta][$index]);
          }
        }
      }
    }
  }

  protected function getLanguages($field) {
    $available_languages = field_available_languages($this->type, $field);
    return _field_language_suggestion($available_languages, static::$options['language'], $field['field_name']);
  }

  protected function validateInstance($field, $instance, $language) {
    $name = $field['field_name'];
    $items = &$this->entity->{$name}[$language];

    if (FALSE == isset($items)) {
      return;
    }

    $settings = $this->mergeSettings($field, $instance);
    $error = &$this->errors[$name][$language];

    foreach ($items as $delta => $item) {
      $amount = $item['amount'];

      if (('' == $amount) || (FALSE == is_numeric($amount))) {
        continue;
      }

      $settings['delta'] = $delta;
      $settings['value'] = Decimal::create($amount, $field['settings']['scale']);
      $this->validateMin($settings, $error);
      $this->validateMax($settings, $error);
    }
  }

  protected function mergeSettings($field, $instance) {
    $settings = $field['settings'] + ['label' => $instance['label']];

    foreach (['min', 'max'] as $item) {
      $value = $instance['settings'][$item];

      if (is_numeric($value)) {
        $settings[$item] = $value;
      }
    }

    return $settings;
  }

  protected function validateMin(array $settings, &$error) {
    /** @var Decimal\Base $value */
    $value = $settings['value'];
    $min = $settings['min'];

    if ((FALSE == is_numeric($min)) || $value->compareWith('>=', $min)) {
      return;
    }

    $error[$settings['delta']][] = [
      'error' => 'money_min',
      'message' => t('%name: the value may be no less than %min.', ['%name' => $settings['label'], '%min' => $min]),
    ];
  }

  protected function validateMax(array $settings, &$error) {
    /** @var Decimal\Base $value */
    $value = $settings['value'];
    $max = $settings['max'];

    if ((FALSE == is_numeric($max)) || $value->compareWith('<=', $max)) {
      return;
    }

    $error[$settings['delta']][] = [
      'error' => 'money_max',
      'message' => t('%name: the value may be no greater than %max.', ['%name' => $settings['label'], '%max' => $max]),
    ];
  }

  static function build($entity_type, $entity, &$errors) {
    return new static($entity_type, $entity, $errors);
  }

  protected static function getHookMap() {
    $class = get_called_class();
    $module = static::getType()->getModule();
    $hooks = [];
    $items = ['field_attach_validate'];

    foreach ($items as $item) {
      $hooks[$class]['hook_' . $item] = $module . '_' . $item;
    }

    return $hooks;
  }

}
